package repository

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPackageReturnsProperFilename(t *testing.T) {
	pkg := Package{
		Name:    "test-package",
		Version: "1.2.3-r0",
	}

	assert.Equal(t, "test-package-1.2.3-r0.apk", pkg.Filename())
}
