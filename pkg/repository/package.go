package repository

import "fmt"

//Package represents a single package with the information present in an
//APKINDEX.
type Package struct {
	Name         string
	Version      string
	Arch         string
	Description  string
	License      string
	Origin       string
	Maintainer   string
	URL          string
	Checksum     []byte
	Dependencies []string
	Provides     []string
}

//Returns the package filename as it's named in a repository.
func (p *Package) Filename() string {
	return fmt.Sprintf("%s-%s.apk", p.Name, p.Version)
}
