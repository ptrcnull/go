package repository

import (
	"archive/tar"
	"bufio"
	"compress/gzip"
	"encoding/base64"
	"fmt"
	"io"
	"strings"
)

type ApkIndex struct {
	Signature   []byte
	Description string
	Packages    []*Package
}

// ParsePackageIndex parses a plain (uncompressed) APKINDEX file. It returns an
// ApkIndex struct
func ParsePackageIndex(apkIndexUnpacked io.ReadCloser) (packages []*Package, err error) {
	defer apkIndexUnpacked.Close()

	indexScanner := bufio.NewScanner(apkIndexUnpacked)

	pkg := &Package{}
	linenr := 1

	for indexScanner.Scan() {
		line := indexScanner.Text()
		if len(line) == 0 {
			if pkg.Name != "" {
				packages = append(packages, pkg)
			}
			pkg = &Package{}
			continue
		}

		if len(line) > 1 && line[1:2] != ":" {
			return nil, fmt.Errorf("Cannot parse line %d: expected \":\" in not found", linenr)
		}

		token := line[:1]
		val := line[2:]

		switch token {
		case "P":
			pkg.Name = val
		case "V":
			pkg.Version = val
		case "A":
			pkg.Arch = val
		case "L":
			pkg.License = val
		case "T":
			pkg.Description = val
		case "o":
			pkg.Origin = val
		case "m":
			pkg.Maintainer = val
		case "U":
			pkg.URL = val
		case "D":
			pkg.Dependencies = strings.Split(val, " ")
		case "p":
			pkg.Provides = strings.Split(val, " ")
		case "C":
			// Handle SHA1 checksums:
			if strings.HasPrefix(val, "Q1") {
				checksum, err := base64.StdEncoding.DecodeString(val[2:])
				if err != nil {
					return nil, err
				}
				pkg.Checksum = checksum
			}
		}

		linenr++
	}

	return
}

func IndexFromArchive(archive io.ReadCloser) (apkindex *ApkIndex, err error) {
	gzipReader, err := gzip.NewReader(archive)
	defer gzipReader.Close()

	if err != nil {
		return
	}

	tarReader := tar.NewReader(gzipReader)
	apkindex = &ApkIndex{}

	for {
		hdr, tarErr := tarReader.Next()

		if tarErr == io.EOF {
			break
		}

		if tarErr != nil {
			return nil, tarErr
		}

		switch hdr.Name {
		case "APKINDEX":
			apkindex.Packages, err = ParsePackageIndex(io.NopCloser(tarReader))
			if err != nil {
				return
			}
		case "DESCRIPTION":
			description, err := io.ReadAll(tarReader)
			if err != nil {
				return nil, err
			}
			apkindex.Description = string(description)
		default:
			if strings.HasPrefix(hdr.Name, ".SIGN.") {
				apkindex.Signature, err = io.ReadAll(tarReader)
			} else {
				return nil, fmt.Errorf("Unexpected file found in APKINDEX: %s", hdr.Name)
			}
		}
	}

	return apkindex, nil
}
