package apkbuild

import (
	"bufio"
	"bytes"
	"io"

	"gopkg.in/yaml.v2"
)

type secfixesSection struct {
	Secfixes Secfixes
}

type Secfixes map[string][]string

func ParseSecfixes(apkbuild io.ReadCloser) (secfixes Secfixes, err error) {
	defer apkbuild.Close()
	apkbuildScanner := bufio.NewScanner(apkbuild)

	secfixYaml := bytes.Buffer{}
	inSecFixes := false

scanLoop:
	for apkbuildScanner.Scan() {
		line := apkbuildScanner.Text()

		switch {
		case inSecFixes && (len(line) == 0 || line[0:1] != "#"):
			break scanLoop
		case line == "# secfixes:":
			inSecFixes = true
			fallthrough
		case inSecFixes:
			if len(line) > 1 {
				secfixYaml.WriteString(line[2:])
			}
			secfixYaml.WriteByte('\n')
		}
	}

	secfixesSection := secfixesSection{}
	err = yaml.Unmarshal(secfixYaml.Bytes(), &secfixesSection)

	if err != nil {
		return
	}

	return secfixesSection.Secfixes, nil
}
