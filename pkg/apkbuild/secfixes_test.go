package apkbuild

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParseSecfixes(t *testing.T) {
	f, err := os.Open("testdata/APKBUILD_SECFIXES")
	require := require.New(t)
	assert := assert.New(t)

	require.Nil(err)

	secfixes, err := ParseSecfixes(f)
	require.Nil(err)

	require.Len(secfixes, 2)

	assert.NotNil(secfixes["1.17.2-r0"])
	assert.Len(secfixes["1.17.2-r0"], 2)
	assert.NotNil(secfixes["1.13.4-r0"])
	assert.Len(secfixes["1.13.4-r0"], 1)
}
