package releases

import (
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewFromBuffer(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	file, err := os.Open("testdata/releases.json")
	require.Nil(err)

	buffer, err := io.ReadAll(file)
	require.Nil(err)

	releases, err := NewFromBuffer(buffer)
	require.Nil(err)

	assert.Len(releases.Architectures, 2)
	require.Len(releases.ReleaseBranches, 2)

	firstBranch := releases.ReleaseBranches[0]
	assert.Equal("edge", firstBranch.RelBranch)
	assert.Equal("master", firstBranch.GitBranch)
	assert.Len(firstBranch.Repos, 3)

	secondBranch := releases.ReleaseBranches[1]
	assert.Equal("v3.13", secondBranch.RelBranch)
	assert.Equal("3.13-stable", secondBranch.GitBranch)
	assert.Equal("2022-11-01", secondBranch.Repos[1].EOLDate)
}
