package releases

// Package releases provides functions and structures to parse information about
// Alpine Linux releases, which is published at https://alpinelinux.org/releases.json.
//
//	releases, err := NewFromURL("https://alpinelinux.org/releases.json.")
//	if err != nil {
//		// Handle error
//	}
//	fmt.Println(releases.GetRelBranch("v3.13").EolDate)

import (
	"encoding/json"
	"io"
	"net/http"
)

type (
	Repo struct {
		Name    string `json:"name"`
		EOLDate string `json:"eol_date,omitempty"`
	}

	Release struct {
		Notes   string `json:"notes"`
		Version string `json:"version"`
		Date    string `json:"date"`
	}

	ReleaseBranch struct {
		Arches     []string  `json:"arches"`
		BranchDate string    `json:"branch_date"`
		EolDate    string    `json:"eol_date"`
		GitBranch  string    `json:"git_branch"`
		RelBranch  string    `json:"rel_branch"`
		Releases   []Release `json:"releases"`
		Repos      []Repo    `json:"repos"`
	}

	Releases struct {
		Architectures   []string        `json:"architectures"`
		ReleaseBranches []ReleaseBranch `json:"release_branches"`
	}
)

// NewFromBuffer parses releases.json from a buffer and returns a Releases
// object if the data could be sucessfully parsed
func NewFromBuffer(buffer []byte) (releases *Releases, err error) {
	releases = &Releases{}
	err = json.Unmarshal(buffer, releases)
	return
}

// NewFromURL can be used to parse releases.json directly from a url
func NewFromURL(url string) (releases *Releases, err error) {
	resp, err := http.Get(url)

	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	releases, err = NewFromBuffer(body)
	return
}

// Fetch downloads and parses the Alpine Linux releases published at
// https://alpinelinux.org/releases.json
func Fetch() (releases *Releases, err error) {
	releases, err = NewFromURL("https://alpinelinux.org/releases.json")
	return
}

// GerRelBranch returns the ReleaseBranch for a certain versionk
// For example:
// 	releases := Fetch()
// 	relBranch := releases.GetRelBranch("v3.13")
func (r *Releases) GetRelBranch(relBranch string) (rb *ReleaseBranch) {
	for _, release := range r.ReleaseBranches {
		if release.RelBranch == relBranch {
			return &release
		}
	}

	return nil
}
