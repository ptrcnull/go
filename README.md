# Alpine Go

These are a set of packages that help with interacting with various Alpine Linux
related systems. These things include:

* APKBUILD files
* APKINDEX files
* releas.json

## Packages

In-depth documentation for this package can be found on [pkg.go.dev][go-dev]

### apkbuild

```go
import "gitlab.alpinelinux.org/alpine/go/pkg/apkbuild"
```

Currently only parses secfixes in APKBUILD files.

### apkindex

```go
import "gitlab.alpinelinux.org/alpine/go/pkg/apkindex"
```

Parses APKINDEX files an provides information about all packages in the index.

### releases

```go
import "gitlab.alpinelinux.org/alpine/go/pkg/releases"
```

Provides functions and structures to get information about Alpine Linux releases.

```go
rels, err := releases.Fetch()

for _, releaseBranch := range rels.ReleaseBranches {
    fmt.Printf("Release %s eol: %s\n", releaseBranch.RelBranch, releaseBranch.EolDate)
}
```

## License

MIT

[go-dev]:https://pkg.go.dev/gitlab.alpinelinux.org/alpine/go
